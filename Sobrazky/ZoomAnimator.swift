//
//  Animator.swift
//  Sobrazky
//
//  Created by Marty on 23/12/15.
//
//

import UIKit
import AlamofireImage

class ZoomAnimator: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3333
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let viewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        if viewController.isBeingPresented() {
            
            animateZoomIn(transitionContext)
        } else {
            
            animateZoomOut(transitionContext)
        }
    }
    
    private func countAspectRatioSize(sourceSize: CGSize, targetSize:CGSize) -> CGRect {
        
        let targetAspectRatio = targetSize.width / targetSize.height
        let sourceAspectRatio = sourceSize.width / sourceSize.height
        
        var newX:CGFloat = 0.0, newY:CGFloat = 0.0, newWidth:CGFloat =  0.0, newHeight:CGFloat = 0.0
        
        if (targetAspectRatio > sourceAspectRatio) {
            newHeight =  targetSize.height
            newWidth = newHeight * sourceAspectRatio
            newX = (targetSize.width - newWidth) * 0.5
        }
        else {
            newWidth = targetSize.width
            newHeight = newWidth / sourceAspectRatio
            newY = (targetSize.height - newHeight) * 0.5
        }
        
        return CGRectMake(newX, newY, newWidth, newHeight)
    }
    
    func animateZoomIn(context: UIViewControllerContextTransitioning) {
        
        let rootController = context.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! SImagesTVC
        let navigationController = context.viewControllerForKey(UITransitionContextToViewControllerKey)! as! UINavigationController
        let pagerController = navigationController.topViewController as! SImagePagerVC
        let imageDetailController = (pagerController.pageViewController.viewControllers! as NSArray).firstObject as! SImageDetailVC
        
        let container = context.containerView()
        container!.addSubview(rootController.view)
        container!.addSubview(navigationController.view)
        
        let selectedImageView = rootController.actualSelectedImageView!
        selectedImageView.alpha = 0
        
        let transitionView = UIImageView()
        transitionView.image = selectedImageView.image
        transitionView.contentMode = .ScaleAspectFill
        transitionView.clipsToBounds = true
        
        let cellRect = rootController.theTableView.convertRect(rootController.theTableView.rectForRowAtIndexPath(rootController.selectedIndexPath!), toView: rootController.view)
        transitionView.frame = CGRect(x: 15.0, y: cellRect.origin.y+8, width: cellRect.size.width-30, height: cellRect.size.height-8-38)
        
//        print("final frame x: \(transitionView.frame.origin.x) h:\(transitionView.frame.origin.y) w: \(transitionView.frame.size.width) h:\(transitionView.frame.size.height)")

        container!.addSubview(transitionView)
        
        let finalFrame = context.finalFrameForViewController(navigationController)
        let transitionViewFinalFrame = self.countAspectRatioSize(selectedImageView.image!.size, targetSize: finalFrame.size)
        
        navigationController.view.alpha = 0
        imageDetailController.theImageView.alpha = 0
        
        UIView.animateWithDuration(transitionDuration(context), animations:
            { () -> Void in
                
                rootController.view.alpha = 0
                navigationController.view.alpha = 1
                
                transitionView.frame = transitionViewFinalFrame
                
            }) { (finished) -> Void in
                
                selectedImageView.alpha = 1
                rootController.view.alpha = 1
                imageDetailController.theImageView.alpha = 1
                
                transitionView.removeFromSuperview()
                context.completeTransition(true)
        }
    }
    
    func animateZoomOut(context: UIViewControllerContextTransitioning) {
        
        let navigationController = context.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! UINavigationController
        let pagerController = navigationController.topViewController as! SImagePagerVC
        let imageDetailController = pagerController.pageViewController.viewControllers?.first as! SImageDetailVC

        let rootController = context.viewControllerForKey(UITransitionContextToViewControllerKey)! as! SImagesTVC
 
        let container = context.containerView()
        container!.addSubview(navigationController.view)
        container!.addSubview(rootController.view)
        container!.sendSubviewToBack(rootController.view)
        
        let sourceImageView = pagerController.currentViewController()?.theImageView!
        let destinationImageView = rootController.actualSelectedImageView
 
        let transitionInitialViewFrame = context.containerView()!.convertRect(sourceImageView!.frame, fromView: imageDetailController.theScrollView)
//        print("final frame x: \(transitionInitialViewFrame.origin.x) h:\(transitionInitialViewFrame.origin.y) w: \(transitionInitialViewFrame.size.width) h:\(transitionInitialViewFrame.size.height)")

        let cellRect = rootController.theTableView.convertRect(rootController.theTableView.rectForRowAtIndexPath(rootController.selectedIndexPath!), toView: rootController.view)
        let finalFrame = CGRect(x: 15.0, y: cellRect.origin.y+8, width: cellRect.size.width-30, height: cellRect.size.height-8-38)
        
        let transitionView = UIImageView(image: destinationImageView!.image)
        transitionView.contentMode = .ScaleAspectFill
        transitionView.clipsToBounds = true
        transitionView.frame = transitionInitialViewFrame
        container!.addSubview(transitionView)
        
        rootController.view.alpha = 0
        imageDetailController.theImageView.alpha = 0
        
        UIView.animateWithDuration(transitionDuration(context), animations:
            { () -> Void in
                
            rootController.view.alpha = 1
            navigationController.view.alpha = 0

            transitionView.frame = finalFrame
            
            }) { (finished) -> Void in
                
                rootController.view.alpha = 1
                navigationController.view.alpha = 1
                
                transitionView.removeFromSuperview()
                context.completeTransition(true)
        }
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
}
