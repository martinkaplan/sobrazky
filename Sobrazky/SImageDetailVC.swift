//
//  SImageDetailVC.swift
//  Sobrazky
//
//  Created by Marty on 23/12/15.
//
//

import Foundation
import UIKit
import AlamofireImage

class SImageDetailVC: UIViewController {
    
    
    @IBOutlet weak var theScrollView: UIScrollView!
    var theImageView: UIImageView!
    
    var image: SImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor()
        
        self.theImageView = UIImageView(frame: CGRectMake(0, 0, CGFloat(self.image.width), CGFloat(self.image.height)))
        
        self.theImageView.af_setImageWithURL(NSURL(string: image.imageUrl)!, placeholderImage: nil, filter: nil, imageTransition: .None)
        
        self.theScrollView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.theScrollView.contentSize = theImageView.bounds.size
        
        self.theScrollView.delegate = self
        
        self.setupGestureRecognizer()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
   //     print("did w: \(self.view.frame.width) h:\(self.view.frame.height)")
        self.theScrollView.subviews.forEach({ $0.removeFromSuperview() })
        self.theScrollView.addSubview(self.theImageView)

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setZoomScale()
    }
    
    func setupGestureRecognizer() {
        let doubleTap = UITapGestureRecognizer(target: self, action: "handleDoubleTap:")
        doubleTap.numberOfTapsRequired = 2
        theScrollView.addGestureRecognizer(doubleTap)
    }
    
    func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        
        if theScrollView.zoomScale > theScrollView.minimumZoomScale {
            theScrollView.setZoomScale(theScrollView.minimumZoomScale, animated: true)
        } else {
            theScrollView.setZoomScale(theScrollView.maximumZoomScale, animated: true)
        }
    }
    
    func setZoomScale() {
        let imageViewSize = theImageView.bounds.size
        let scrollViewSize = theScrollView.bounds.size
        
        //print("image w: \(imageViewSize.width) h:\(imageViewSize.height)")
       // print("scroll w: \(scrollViewSize.width) h:\(scrollViewSize.height)")
        
        let widthScale = scrollViewSize.width / imageViewSize.width
        let heightScale = scrollViewSize.height / imageViewSize.height
        
        theScrollView.minimumZoomScale = min(widthScale, heightScale)
        theScrollView.maximumZoomScale = 4
        theScrollView.zoomScale = theScrollView.minimumZoomScale
        
//        print("min zoom scale:\(theScrollView.minimumZoomScale)")
    }
    
}

extension SImageDetailVC:UIScrollViewDelegate{
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.theImageView
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        let imageViewSize = theImageView.frame.size
        let scrollViewSize = theScrollView.bounds.size
        
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
}
