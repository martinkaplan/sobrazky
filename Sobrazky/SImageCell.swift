//
//  SImageCell.swift
//  Sobrazky
//
//  Created by Marty on 22/12/15.
//
//

import UIKit
import AlamofireImage

class SImageCell: UITableViewCell {
    
    @IBOutlet weak var theImageView: UIImageView!
    @IBOutlet weak var theNameLabel: UILabel!
    @IBOutlet weak var theSizeLabel: UILabel!
    
    var sImage:SImage?{
        didSet{
            setUpCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None    
    }
    
    func setUpCell() {
        if sImage != nil{
            self.theNameLabel?.text = sImage?.title
            self.theSizeLabel?.text = "\(sImage!.width)x\(sImage!.height)"
            
            let placeholder = UIImage(named:"placeholder")
            self.theImageView.af_setImageWithURL(
                NSURL(string: sImage!.imageUrl)!,
                placeholderImage: placeholder,
                filter: nil,
                imageTransition: .FlipFromBottom(0.333)
            )
        }
    }
    
}
