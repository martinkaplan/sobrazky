//
//  LoadingCell.swift
//  Sobrazky
//
//  Created by Marty on 22/12/15.
//
//

import UIKit

class LoadingCell: UITableViewCell {
    
    @IBOutlet weak var theActivityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
    }
    
    
}
