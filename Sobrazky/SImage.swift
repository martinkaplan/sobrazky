//
//  Sobrazek.swift
//  Sobrazky
//
//  Created by Marty on 22/12/15.
//
//

import Foundation
import UIKit
import SwiftyJSON


public struct SImage :Equatable{
    
    
    let thumbHeight:Int
    let thumbUrl:String
    let title:String
    let imageUrl:String
    let detailHeight:Int
    
    let height:Int
    let resultNumber:Int
    let width:Int
    let detailWidth:Int
    
    let pageUrl:String
    let pageUrlShort:String
    let imageUrlShort:String
    let detailUrl:String
    let identifier:String
    let thumbWidth:Int
    
    var ratio:CGFloat{
        get{
            return CGFloat(self.width)/CGFloat(self.height)
        }
    }
 
    init(json: SwiftyJSON.JSON){
       // print(json)
        
        if let tmp = json["thumbHeight"].int{
            self.thumbHeight = tmp
        }else{
            self.thumbHeight = 0
        }
        
        if let tmp = json["thumbUrl"].string{
            self.thumbUrl = tmp.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        }else{
            self.thumbUrl = ""
        }
        
        if let tmp = json["title"].string{
            self.title = tmp
        }else{
            self.title = ""
        }
        
        if let tmp = json["imageUrl"].string{
            self.imageUrl = tmp.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        }else{
            self.imageUrl = ""
        }
        
        if let tmp = json["detailHeight"].int{
            self.detailHeight = tmp
        }else{
            self.detailHeight = 0
        }
        
        if let tmp = json["height"].int{
            self.height = tmp
        }else{
            self.height = 0
        }
        
        if let tmp = json["resultNumber"].int{
            self.resultNumber = tmp
        }else{
            self.resultNumber = 0
        }
        
        if let tmp = json["width"].int{
            self.width = tmp
        }else{
            self.width = 0
        }
        
        if let tmp = json["detailWidth"].int{
            self.detailWidth = tmp
        }else{
            self.detailWidth = 0
        }
        
        if let tmp = json["pageUrl"].string{
            self.pageUrl = tmp.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        }else{
            self.pageUrl = ""
        }
        
        if let tmp = json["pageUrlShort"].string{
            self.pageUrlShort = tmp.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        }else{
            self.pageUrlShort = ""
        }
        
        if let tmp = json["imageUrlShort"].string{
            self.imageUrlShort = tmp.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        }else{
            self.imageUrlShort = ""
        }
        
        if let tmp = json["imageUrl"].string{
            self.detailUrl = tmp.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        }else{
            self.detailUrl = ""
        }
        
        if let tmp = json["id"].string{
            self.identifier = tmp
        }else{
            self.identifier = ""
        }
        
        if let tmp = json["thumbWidth"].int{
            self.thumbWidth = tmp
        }else{
            self.thumbWidth = 0
        }
    }
    
}


public func ==(lhs: SImage, rhs: SImage) -> Bool{
    return lhs.identifier == rhs.identifier
}
