//
//  SImagesTVC.swift
//  Sobrazky
//
//  Created by Marty on 22/12/15.
//
//

//import AlamofireImage
import Foundation
import UIKit


class SImagesTVC: UIViewController {
    
    internal struct ClassConstants {
        static let sImageCellIdentifier = "SImageCell"
        static let loadingCellIdentifier = "LoadingCell"
        
        static let segueToImageDetail = "segueToImageDetail"
    }
    
    @IBOutlet weak var theTableView: UITableView!
    @IBOutlet weak var theSearchBar: UISearchBar!
    
    
    private var animator:ZoomAnimator = ZoomAnimator()
    var actualSelectedImageView: UIImageView?
    var selectedIndexPath: NSIndexPath?
    
    lazy var images:[SImage] = [SImage]()
    
    private var sImagePage:Int = 0
    private var loadingMoreImages:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.theTableView.tableFooterView = UIView()
        self.theTableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == ClassConstants.segueToImageDetail {
            
            if let navigationController:UINavigationController = segue.destinationViewController as? UINavigationController{
                
                if let paggerController:SImagePagerVC = navigationController.viewControllers[0] as? SImagePagerVC{
                    
                    let indexPath = self.theTableView.indexPathForSelectedRow?.row ?? 0
                    paggerController.startSImage = self.images[indexPath]
                    
                    paggerController.imagePagerDataSource = self
                    navigationController.transitioningDelegate = animator
                }
            }
        }
        
    }
    
}

extension SImagesTVC:UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        if indexPath.row < self.images.count{
            
            self.selectedIndexPath = indexPath
            
            let imageCell =  self.tableView(tableView, cellForRowAtIndexPath: indexPath) as! SImageCell
            self.actualSelectedImageView = imageCell.theImageView
            
            self.performSegueWithIdentifier(ClassConstants.segueToImageDetail, sender: self)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row < self.images.count{
            
            let sImg:SImage = self.images[indexPath.row]
            let imgWidth = self.theTableView.frame.size.width-15-15
            return  8.0 + CGFloat(imgWidth/sImg.ratio) + 38.0 //to do const?
        }else{
            
            return 55
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if images.count == 0{
            return 0
        }
        return images.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row < self.images.count{
            
            let cell = tableView.dequeueReusableCellWithIdentifier(ClassConstants.sImageCellIdentifier) as! SImageCell
            
            let sImg:SImage = self.images[indexPath.row]
            cell.sImage = sImg
            
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCellWithIdentifier(ClassConstants.loadingCellIdentifier) as! LoadingCell
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if cell.isKindOfClass(LoadingCell){
            self.loadMoreImages()
        }
    }
    
    
}

// MARK: - Search bar delegate
extension SImagesTVC :UISearchBarDelegate{
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        images.removeAll()
        self.theTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        self.theSearchBar.endEditing(true)
        
        images.removeAll()
        self.theTableView.reloadData()
        
        self.sImagePage = 0
        
        SImageBrain.getImagesForString(theSearchBar.text!, sImagesProtocol: self, page:self.sImagePage)
        loadingMoreImages = true
    }
    
}

// MARK: - SImageProtocol
extension SImagesTVC:SImagesProtocol{
    
    private func loadMoreImages(){
        loadingMoreImages = true
        SImageBrain.getImagesForString(theSearchBar.text!, sImagesProtocol: self, page:++self.sImagePage)
    }
    
    func addImages(newImages:[SImage]){
        
        loadingMoreImages = false
        self.images.appendContentsOf(newImages)
        self.theTableView.reloadData()
    }
    
    func error(description:String){
        
        loadingMoreImages = false
        
        let alertController = UIAlertController(title: "Chyba", message:description, preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .Cancel) {
            (action) in
        }
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true) {}
    }
    
}

// MARK: - SImagePagerDataSource
extension SImagesTVC:SImagePagerDataSource{
    
    func actualSImageChanged(image: SImage){
        
        if let imgIndex = images.indexOf(image) {
            
            let indexPath = NSIndexPath(forRow: imgIndex, inSection: 0)
            
            let imageCell = self.tableView(self.theTableView, cellForRowAtIndexPath: indexPath) as? SImageCell
            self.actualSelectedImageView = imageCell?.theImageView
            
            self.theTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Middle, animated: false)
        }
    }
    
    func nextSImage(image:SImage)->SImage?{
        
        if let index = findImageIndex(image) {
            
            let nextImageIndex = index + 1
            
            if (index + 5) == self.images.count{
                if !self.loadingMoreImages{
                    self.loadMoreImages()
                }
            }
            
            if nextImageIndex >= self.images.endIndex {
                return nil
            }
            
            return self.images[nextImageIndex]
        }
        return nil
    }
    
    func previousSImage(image:SImage)->SImage?{
        
        if let index = findImageIndex(image) {

            let previousIndex = index - 1
            if previousIndex > 0 {
                return self.images[previousIndex]
            }
        }
        return nil
    }
    
    private func findImageIndex(image: SImage) -> Int? {
        
        let imageIndex = self.images.indexOf(image)
        return imageIndex
    }
}
