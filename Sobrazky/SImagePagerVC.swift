//
//  SImagePagerVC.swift
//  Sobrazky
//
//  Created by Marty on 28/12/15.
//
//

import Foundation
import UIKit

protocol SImagePagerDataSource:class {
    func actualSImageChanged(image: SImage)
    
    func nextSImage(image:SImage)->SImage?
    func previousSImage(image:SImage)->SImage?
}

class SImagePagerVC: UIViewController{
    
    //var images:[SImage]! = [SImage]()
    var startSImage:SImage!
    weak var imagePagerDataSource:SImagePagerDataSource?
    
    var pageViewController:UIPageViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.pageViewController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        
        let firstVC = self.viewControllerForSImage(self.startSImage)
        self.pageViewController.setViewControllers([firstVC], direction: .Forward, animated: false, completion: nil)
        
        self.view.addSubview(self.pageViewController.view)
        
        let recognizer = UITapGestureRecognizer(target: self, action: Selector("toggleNavBarVisibility"))
        view.addGestureRecognizer(recognizer)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.pageViewController.view.frame = self.view.frame
        
        //        print("pager will w: \(self.view.frame.width) h:\(self.view.frame.height)")
        if let detailVC = self.pageViewController.viewControllers?.first as? SImageDetailVC{
            detailVC.theScrollView.frame = self.view.frame
            detailVC.setZoomScale()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        
        if let navigationController = self.navigationController {
            if !navigationController.navigationBarHidden{
                return .LightContent
            }
        }
        return .Default
    }
    
    // MARK: - Actions
    @IBAction func doneButtonPressed(sender: AnyObject) {
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func shareButtonPressed(sender: AnyObject) {
        
        let imageDetailVC = self.pageViewController.viewControllers?.last as! SImageDetailVC
        
        if let image = imageDetailVC.image{
            
            let textToShare = image.title
            if let imgUrl = NSURL(string: image.imageUrl){
                let objectsToShare = [textToShare, imgUrl]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]
                
                self.presentViewController(activityVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func toggleNavBarVisibility() {
        
        if let navigationController = self.navigationController {
            let newState = !navigationController.navigationBarHidden
            navigationController.setNavigationBarHidden(newState, animated: true)
        }
    }
    
}

extension SImagePagerVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    
    func viewControllerForSImage(image: SImage) -> SImageDetailVC {
        
        let imageDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SImageDetailVC") as! SImageDetailVC
        imageDetailVC.image = image

        imageDetailVC.view.frame = self.view.frame
        imageDetailVC.setZoomScale()
        
        return imageDetailVC
    }
    
    func currentViewController() -> SImageDetailVC? {
        return self.pageViewController.viewControllers?.first as? SImageDetailVC
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let photoController = viewController as! SImageDetailVC
        if let image = photoController.image {
            
            if let nextImage = imagePagerDataSource?.nextSImage(image){
                return self.viewControllerForSImage(nextImage)
            }
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let photoController = viewController as! SImageDetailVC
        if let image = photoController.image {
            
            if let previousImage = imagePagerDataSource?.previousSImage(image){
                return self.viewControllerForSImage(previousImage)
            }
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if let controller = currentViewController() {
            imagePagerDataSource?.actualSImageChanged(controller.image!)
        }
    }
    
}