//
//  SImageBrain.swift
//  Sobrazky
//
//  Created by Marty on 22/12/15.
//
//

import Foundation
import Alamofire
import SwiftyJSON


protocol SImagesProtocol{
    
    func addImages(images:[SImage])
    func error(description:String)

}

class SImageBrain:NSObject{
    
    
    static func getImagesForString(searchString:String, sImagesProtocol:SImagesProtocol, page:Int){
        
        let step:Int = 24
        let parameters = [
            "q":searchString,
            "step":step.description,
            "size":"any",
            "color":"any",
            "from":(page*step).description]
        
        Alamofire.request(.GET, "http://www.obrazky.cz/searchAjax", parameters:parameters).responseJSON
            { response  in
                
                print("search request completed")
                
                if response.result.isSuccess{
                    let json = JSON(response.result.value as! NSDictionary)
                    //  print(json)
                    
                    var retImages:[SImage] = [SImage]()
                    if let images = json["metadata"].array{
                        for img in images{
                            retImages.append(SImage(json: img))
                        }
                    }

                    sImagesProtocol.addImages(retImages)
                    
                }else{
                    sImagesProtocol.error("No internet")
                }
        }
    }
    
}